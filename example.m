clear all
close all

Ftip = Fingertip;
Ftip = Init(Ftip,'pressure',3,'friction',0.4);
deep = false;
[U,F,SE,eps] = Run(Ftip,deep); % SE = strain energy
Animate(Ftip,U,F,eps,deep,true)

N = Ftip.Nl;
ac = contactArea(Ftip,U);
delta = normIndent(Ftip,U);
div = divergence(Ftip,U);
SET = sum(SE([2:N+1,N+3:end],:)); % strain energy total (excluding the bone)
%[sedXY,sedXZ] = strainEnergy(Ftip,U);

forceN = abs(sum(F(2:N+1,:),1));
figure;
subplot(1,2,1); hold on
plot(forceN,ac)
ylabel('contact area')
subplot(1,2,2); hold on
plot(forceN,SET)
ylabel('strain energy (mJ/mm3)')

% save(['../Stiffness/' num2str(Ftip.kc/Ftip.d*2*1e-3) 'kPa.mat'],'U','F','ac','SET')
% save(['../Friction/' num2str(Ftip.fclb) '.mat'],'U','F','ac','SET')
% save(['../Curvature/' num2str(Ftip.Rcurv) '.mat'],'U','F','ac','SET')

