
classdef Fingertip
    properties
        % % Skin parameters
        E_ext = 1.54e6;          %% young's modulus (Pa) of the epidermis
        d = 1e-2;                %% diameter of the contact area
        e_ext = 20e-6;           %% thickness of the external layer
        E_tis = 0.025e6;         %% young's modulus (Pa) of the skin tissue
        nu = 0.4;                %% poisson's ratio (Fung)
        
        % % Skin structure
        r = 8e-3;                %% finger radius in m
        Nl = 501;                %% number of elements
        
        % % Damping
        b = 0.1;
        
        % % Sampling frequency
        Fs = 6e5;
        
        % % External forces applied on the bone
        P = 2;                   %% initial pressure
        Q = 0;                   %% initial traction
        
        % % Surface properties
        kc = 1e4;                %% stiffness of the surface (Pa*m=N/m)
        fclb = 0.4;              %% coulomb friction coefficient
        Rcurv = 0;               %% radius of curvature (m)
        
        % % Friction modeling
        sigma0 = 1e4;             %% rest stiffness of the bristle (N/m)
        n = 0.7;                  %% exponent: material dependent parameter
        
    end
    methods
        function Ftip = Init(Ftip,varargin)
            global kext kt kn Kn B x0 l0
            
            N = Ftip.Nl;
            
            %% Change the properties of the surface
            p = inputParser;
            addOptional(p,'pressure',2,@(x) validateattributes(x,{'numeric'},{'positive'}));
            addOptional(p,'traction',0,@(x) validateattributes(x,{'numeric'},{'finite'}));
            addOptional(p,'stiffness',2e6,@(x) validateattributes(x,{'numeric'},{'positive'}));
            addOptional(p,'friction',0.4,@(x) validateattributes(x,{'numeric'},{'positive'}));
            validationFcn = @(x) isnumeric(x) && or(x<-Ftip.r, x>=0);
            addOptional(p,'curvature',0,validationFcn);
            parse(p,varargin{:});
            
            Ftip.P = p.Results.pressure;
            Ftip.Q = p.Results.traction;
            Ftip.kc = p.Results.stiffness*Ftip.d/2;
            Ftip.fclb = p.Results.friction;
%             Ftip.nu = min(p.Results.friction,0.48);
            Ftip.Rcurv = p.Results.curvature;
            
            %% Initial position
            theta = linspace(-pi,0,N+2);   %%semicircle
            [x,y] = pol2cart(theta(2:end-1),Ftip.r*ones(size(theta(2:end-1))));
            d0 = 1e-5;                           %% initial distance from the surface
            y = y+d0+Ftip.r;                     %%vertical shift
            x0 = [d0+Ftip.r,y,0,x]';
            dSkin = sqrt(diff(x0(2:N+1)).^2+diff(x0(N+3:end)).^2);   %%Length of each membrane segment
            l0 = mean(dSkin);   %%inital length membrane elements
            
            %% Matrix initialization
            B = -Ftip.b*speye(2*(N+1));        %% damping matrix
            
            % % Spring stiffnesses
            kext = Ftip.E_ext*Ftip.d*Ftip.e_ext/l0/2;        %% normal spring stiffness = young modulus
            kt = Ftip.E_tis*pi*Ftip.r^2/2/Ftip.r;          %% stiffness of the spring which impose the shape
            kn = 5e3;
            
            idx_i =  [1, 2, N+1, 1,2, 1,   N+1];
            idx_j =  [1, 1, 1,   2,2, N+1, N+1];
            values = [-2,1, 1,   1,-1,1,   -1];
            Kn = sparse(idx_i,idx_j,values,2*(N+1),2*(N+1));
            
        end
        function [dU,SE] = f_U(Ftip,Uc,Fc)
            global kext kt kn Kn B x0
            N = Ftip.Nl;
            pos_x = Uc(N+2:2*(N+1))+x0(N+2:2*(N+1));
            pos_z = Uc(1:N+1)+x0(1:N+1);
            
            part_int = [-ones(1,floor(N/2)),0,ones(1,floor(N/2))];
            part_ext = [-ones(1,floor(N/2)),ones(1,floor(N/2))];
            
            %Stiffness matrix (spring)
            % Kf: Springs dependencies of the membrane
            angleA = atan(diff(pos_z(1:N+1))./diff(pos_x(1:N+1))); %% angle between 2 elements and the horizontal spring
            Acos = abs(cos(angleA(1:N)))';
            Asin = abs(sin(angleA(1:N)))';
            
            %Boundary conditions
            Acos(1,1) = 0; Asin(1,1) = 0;
            Acos(1,N+1) = 0; Asin(1,N+1) = 0;
            
            d0 = -[0,Asin(1:N)+Asin(2:N+1),0,Acos(1:N)+Acos(2:N+1)]';
            d0sub = [0,Asin(2:N),0,0,Acos(2:N),0]';        %% sous-diagonale
            d0sup = [0,0,Asin(2:N),0,0,Acos(2:N)]';        %% sur-diagonale
            d12 = -[0,[0,part_ext].*Acos(1:N)+[part_ext,0].*Acos(2:N+1)]';
            d12sub = [0,part_ext.*Acos(2:N),0]';
            d12sup = [0,0,part_ext.*Acos(2:N)]';
            d21 = -[0,[0,part_ext].*Asin(1:N)+[part_ext,0].*Asin(2:N+1)]';
            d21sub = [0,part_ext.*Asin(2:N),0]';
            d21sup = [0,0,part_ext.*Asin(2:N)]';
            spKd = (spdiags([d0sub d0 d0sup],-1:1,length(d0),length(d0)))+...
                Ftip.nu/100*(spdiags([zeros(N+1,3);[d12sub d12 d12sup]],N:N+2,length(d0),length(d0)))+...
                Ftip.nu/100*(spdiags([[d21sub d21 d21sup];zeros(N+1,3)],-N-2:-N,length(d0),length(d0)));
            
            % Ksh: Springs dependencies of the subcutaneous tissues
            angleB = atan(abs(pos_z(1)-pos_z(2:N+1))./abs(pos_x(1)-pos_x(2:N+1))); %% angle between two elements linked to the bone
            Bcos = abs(cos(angleB(1:N)))';
            Bsin = abs(sin(angleB(1:N)))';
            
            v11 = [-sum(Bsin),Bsin];
            v22 = [-sum(Bcos),Bcos];
            v12 = [sum(part_int.*Bcos(1:N)),-part_int.*Bcos(1:N)];
            v21 = [sum(part_int.*Bsin(1:N)),-part_int.*Bsin(1:N)];
            
            spKsh = (sparse(1,1:N+1,v11,2*(N+1),2*(N+1))+...
                sparse(2:N+1,1,Bsin,2*(N+1),2*(N+1))+...
                sparse(N+2,N+2:2*(N+1),v22,2*(N+1),2*(N+1))+...
                sparse(N+3:2*(N+1),N+2,Bcos,2*(N+1),2*(N+1))+...
                spdiags([0,-Bsin,0,-Bcos]',0,2*(N+1),2*(N+1)))+...
                Ftip.nu*(sparse(1,N+2:2*(N+1),v12,2*(N+1),2*(N+1))+...
                sparse(2:N+1,N+2,-part_int.*Bcos,2*(N+1),2*(N+1))+...
                spdiags([zeros(1,N+1),0,part_int.*Bcos]',N+1,2*(N+1),2*(N+1)))+...
                Ftip.nu*(sparse(N+2,1:N+1,v21,2*(N+1),2*(N+1))+...
                sparse(N+3:2*(N+1),1,-part_int.*Bsin,2*(N+1),2*(N+1))+...
                spdiags([0,part_int.*Bsin]',-N-1,2*(N+1),2*(N+1)));
            
            % Sum of springs dependancies matrix
            spK = kext.*spKd+kt.*spKsh+kn.*Kn;
            
            dU = -inv(B)*spK*Uc-inv(B)*Fc;
            
            SE = 1/2*spK*Uc.^2;
        end
        
        function [U,F,SE,eps] = Run(Ftip,deep)
            global x0 l0
            %% Time vector
            dt = 1/Ftip.Fs;                %% sampling period
            time = 0:dt:0.5;          %% time vector
            Ndt = length(time);
            t = 2;
            
            %% Surface
            surface = @(x) (sign(Ftip.Rcurv)*(sqrt(abs(Ftip.Rcurv)^2-x.^2)-abs(Ftip.Rcurv))).*(Ftip.Rcurv~=0) +...
                0.*(Ftip.Rcurv~=0);
            
            %% Initial force and displacement matrices
            N = Ftip.Nl;
            U = zeros(2*(N+1),length(time));    %% displacement U = [UN UT]
            
            F = zeros(2*(N+1),length(time));    %% force F = [FN FT]
            F(:,1:Ndt) = repmat([-Ftip.P;zeros(N,1);0;zeros(N,1)],1,Ndt);
            
            Upt = zeros(N+1,1);                 %% speed tangential to the surface
            pos = x0;                           %% position of all elements
            
            ndraw = 500;
            X = x0(N+3):1e-4:x0(end); Zv = (-4e-3:2e-4:-1e-4)';
            [Xq,Zq]=meshgrid(X,Zv);
            tic
            while(t<=2 || abs(sum(F(1:N+1,t-1),1))>10e-2*Ftip.P)      %% Stop condition: global net force <5%
                if(t>2)
                    Upt = (U(N+2:2*(N+1),t-1)-U(N+2:2*(N+1),t-2))/(dt);    %% speed along x
                end
                % % Contact detection
                ind = find(pos(1:N+1) - surface(pos(N+2:2*(N+1))) <0);
                if(~isempty(ind))
                    % % Force resolution
                    Fr = Ftip.kc*abs(pos(ind)- surface(pos(ind+N+1)));
                    F(ind,t) = F(ind,t) + Fr;
                    % Friction modeling
                    % Dahl model
                    F(ind+N+1,t) = F(ind+N+1,t) + F(ind+N+1,t-1) - dt*...
                        Ftip.sigma0*Upt(ind).*abs(1+F(ind+N+1,t-1)./(Ftip.fclb*Fr.*sign(Upt(ind)))).^Ftip.n.*...
                        sign(1+F(ind+N+1,t-1)./(Ftip.fclb*Fr).*sign(Upt(ind)));
                    indZ = find(Upt(ind)==0);
                    if(~isempty(indZ)) % central element
                        F(ind(indZ)+N+1,t) = 0;
                    end
                end
                
                % % Displacement resolution (Runge-Kutta 4)
                k1 = f_U(Ftip,U(:,t-1),F(:,t));
                k2 = f_U(Ftip,U(:,t-1)+0.5*dt*k1,F(:,t));
                k3 = f_U(Ftip,U(:,t-1)+0.5*dt*k2,F(:,t));
                [k4,SE_t] = f_U(Ftip,U(:,t-1)+dt*k3,F(:,t));
                U(:,t) = U(:,t-1)+ dt/6*(k1+2*k2+2*k3+k4);
                SE(:,t) = SE_t; %mJ/mm^3
                pos = U(:,t) + x0;
                
                if(deep == true)
                if(~mod(t,ndraw))
                    %% Strains at the depth of the mechanoreceptors
                    p(:,t-1) = F(2:N+1,t-1)./(l0*Ftip.d);
                    q(:,t-1) = F(N+3:2*(N+1),t-1)./(l0*Ftip.d);
                    pf = @(s) p(find(abs(x0(N+3:2*(N+1))-s)==min(abs(x0(N+3:2*(N+1))-s))),t-1);
                    qf = @(s) q(find(abs(x0(N+3:2*(N+1))-s)==min(abs(x0(N+3:2*(N+1))-s))),t-1);
                    
                    sigma_x = -2/pi*Zq.*integral(@(s) pf(s).*(Xq-s).^2./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true)-...
                        -2/pi.*integral(@(s) qf(s).*(Xq-s).^3./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true);
                    sigma_z = -2/pi*Zq.^3.*integral(@(s) pf(s)./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true)-...
                        -2/pi*Zq.^2.*integral(@(s) qf(s).*(Xq-s)./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true);
%                     tau = @(x) -2/pi*Zq.^2.*integral(@(s) pf(s).*(Xq-s).^2./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true)-...
%                         -2/pi*Zq.*integral(@(s) qf(s).*(Xq-s).^2./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true);
                    eps(:,:,t) = 1/Ftip.E_tis*[sigma_x-Ftip.nu*sigma_z;-Ftip.nu*sigma_x+sigma_z];
                end
                else
                    eps(:,:,t) = zeros(size(Xq));
                end
                t = t+1;
            end
            toc
            if(deep == true)
            %% Strains at the depth of the mechanoreceptors
            tic
            p(:,t-1) = F(2:N+1,t-1)./(l0*Ftip.d);
            q(:,t-1) = F(N+3:2*(N+1),t-1)./(l0*Ftip.d);
            pf = @(s) p(find(abs(x0(N+3:2*(N+1))-s)==min(abs(x0(N+3:2*(N+1))-s))),t-1);
            qf = @(s) q(find(abs(x0(N+3:2*(N+1))-s)==min(abs(x0(N+3:2*(N+1))-s))),t-1);
            
            sigma_x = -2/pi*Zq.*integral(@(s) pf(s).*(Xq-s).^2./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true)-...
                -2/pi.*integral(@(s) qf(s).*(Xq-s).^3./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true);
            sigma_z = -2/pi*Zq.^3.*integral(@(s) pf(s)./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true)-...
                -2/pi*Zq.^2.*integral(@(s) qf(s).*(Xq-s)./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true);
%                 tau = @(x) -2/pi*Zq.^2.*integral(@(s) pf(s).*(Xq-s).^2./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true)-...
%                     -2/pi*Zq.*integral(@(s) qf(s).*(Xq-s).^2./((Xq-s).^2+Zq.^2).^2,x0(N+3),x0(end),'ArrayValued',true);
            eps(:,:,t-1) = 1/Ftip.E_tis*[sigma_x-Ftip.nu*sigma_z;-Ftip.nu*sigma_x+sigma_z];
            toc
            else
                eps(:,:,t) = zeros(size(Xq));
            end
            
            ti = t;
            if(Ftip.Q~=0)
                F(:,ti:Ndt) = repmat([-Ftip.P;zeros(N,1);Ftip.Q;zeros(N,1)],1,Ndt-ti+1);
                while(abs(sum(F(N+3:2*(N+1),t-1),1))<0.9*Ftip.fclb*Ftip.P)
                    Upt = (U(N+2:2*(N+1),t-1)-U(N+2:2*(N+1),t-2))/(dt);    %% speed along x
                    % % Contact detection
                    ind = find(pos(1:N+1) - surface(pos(N+2:2*(N+1))) <0);
                    if(~isempty(ind))
                        % % Force resolution
                        Fr = Ftip.kc*abs(pos(ind)- surface(pos(ind+N+1)));
                        F(ind,t) = F(ind,t) + Fr;
                        % Friction modeling
                        % Dahl model
                        F(ind+N+1,t) = F(ind+N+1,t) + F(ind+N+1,t-1) - dt*...
                            Ftip.sigma0*Upt(ind).*abs(1+F(ind+N+1,t-1)./(Ftip.fclb*Fr.*sign(Upt(ind)))).^Ftip.n.*...
                            sign(1+F(ind+N+1,t-1)./(Ftip.fclb*Fr).*sign(Upt(ind)));
                        indZ = find(Upt(ind)==0);
                        if(~isempty(indZ)) % central element
                            F(ind(indZ)+N+1,t) = 0;
                        end
                    end
                    
                    % % Displacement resolution (Runge-Kutta 4)
                    k1 = f_U(Ftip,U(:,t-1),F(:,t));
                    k2 = f_U(Ftip,U(:,t-1)+0.5*dt*k1,F(:,t));
                    k3 = f_U(Ftip,U(:,t-1)+0.5*dt*k2,F(:,t));
                    [k4,SE_t] = f_U(Ftip,U(:,t-1)+dt*k3,F(:,t));
                    U(:,t) = U(:,t-1)+ dt/6*(k1+2*k2+2*k3+k4);
                    SE(:,t) = SE_t;
                    pos = U(:,t) + x0;
                    
                    t = t+1;
                end
            end
            
            U = U(:,1:t-1);
            F = F(:,1:t-1);
            
        end
        
        function [] = Animate(Ftip,U,F,eps,deep,save)
            global x0
            N = Ftip.Nl;
            %% Surface
            surface = @(x) (sign(Ftip.Rcurv)*(sqrt(abs(Ftip.Rcurv)^2-x.^2)-abs(Ftip.Rcurv))).*(Ftip.Rcurv~=0) +...
                0.*(Ftip.Rcurv~=0);
            vecSurf = -5e-3:1e-5:5e-3;
            surfDef = surface(vecSurf);
            mu = abs(F(N+2:2*(N+1),:)./F(1:N+1,:));
            
            X = x0(N+3):1e-4:x0(end);
            rho=-3.9e-3:2e-4:0; rho = 0:2e-4:3.9e-3;
            
            h = figure('units','normalized','outerposition',[0 0 2/3 1],'Color','w');
            if(save==true)
                filename = ['../GIF/simu_f' num2str(Ftip.fclb) '_c' num2str(Ftip.Rcurv) '_s' num2str(Ftip.kc/5e-3*1e-3) '.gif'];
                frame = getframe(h);
                im = frame2im(frame);
                [imind,cm] = rgb2ind(im,256);
                imwrite(imind,cm,filename,'gif','LoopCount',Inf,'DelayTime',5e-4);
            end
            hold on
            axis off
            ndraw = 500;
            vec_plot = floor((N+1)/2)+1-69:3:floor((N+1)/2)+1+70;
            x_m = [-4.92,-3.69,-2.46,-1.23,0,1.23,2.46,3.69,4.92].*1e-3;
            sz = 30;
            for k=1:size(U,2)
                if (~mod(k,ndraw))
                    if(~deep)
                    clf;
                    subplot 121
                    axis off
                    hold on
                    % % Update surface deformation
                    indC = find(U(1:N+1,k)+x0(1:N+1) - surface(U(N+2:2*(N+1),k)+x0(N+2:2*(N+1)))<=0);
                    if(length(indC)>2)
                        [~,indS] = min(abs(U(indC+N+1,k)+x0(indC+N+1)-vecSurf)');
                        surfDef(min(indS):max(indS)) = interp1(indS,U(indC,k)+x0(indC),min(indS):max(indS));
                    end
                    area(vecSurf,surfDef,-2e-3,'Facecolor','k')
                    
                    cd = [NaN;mu(vec_plot(2:end),k)];
                    cd(isnan(cd)) = zeros(length(cd(isnan(cd))),1);
                    scatter(U(N+1+vec_plot,k)+x0(N+1+vec_plot),U(vec_plot,k)+x0(vec_plot),sz,cd,'filled');
                    colorbar;
                    caxis([0 1])
                    shading flat
                    axis equal
                    axis off
                    
                    quiver(U(N+1+vec_plot(2:end),k)+x0(N+1+vec_plot(2:end)),U(vec_plot(2:end),k)+x0(vec_plot(2:end)),...
                        -F(N+1+vec_plot(2:end),k),-F(vec_plot(2:end),k),0.75,'Color','w','Linewidth',1)
                    
                    xlim([-5e-3 5e-3])
                    xlabel('x (m)')
                    ylim([-1e-3 3.5e-3])
                    ylabel('y (m)')
                    caxis([-1e-3 3e-3])
                    
                    subplot 122
                    hold on
                    pos_x = 1/2*(U(N+1+vec_plot(2:end-1),k)+x0(N+1+vec_plot(2:end-1))+U(N+1+vec_plot(3:end),k)+x0(N+1+vec_plot(3:end)));
                    plot(pos_x.*1e3,diff(U(N+1+vec_plot(2:end),k)))
                    xlabel('position (mm)')
                    ylabel('tangential strain')
                    
                    
                    else
                    clf;
                    subplot 221
                    axis off
                    hold on
                    % % Update surface deformation
                    indC = find(U(1:N+1,k)+x0(1:N+1) - surface(U(N+2:2*(N+1),k)+x0(N+2:2*(N+1)))<=0);
                    if(length(indC)>2)
                        [~,indS] = min(abs(U(indC+N+1,k)+x0(indC+N+1)-vecSurf)');
                        surfDef(min(indS):max(indS)) = interp1(indS,U(indC,k)+x0(indC),min(indS):max(indS));
                    end
                    area(vecSurf,surfDef,-2e-3,'Facecolor','k')
                    
                    z(:,k) = interp1(U(N+3:end,k)+x0(N+3:end),U(2:N+1,k)+x0(2:N+1),X);
                    pcolor(repmat(X,length(rho),1),flipud((z(:,k)+rho)'),eps(1:length(rho),:,k).*1e-3)
                    shading flat
                    axis equal
                    axis off
                    
                    z_m = interp1(U(N+3:end,k)+x0(N+3:end),U(2:N+1,k)+x0(2:N+1)+2e-3,x_m); %%PC densitiy=0.81/cm2
                    scatter(x_m,z_m,20,'k','filled')
                    
                    quiver(U(N+1+vec_plot(2:end),k)+x0(N+1+vec_plot(2:end)),U(vec_plot(2:end),k)+x0(vec_plot(2:end)),...
                        -F(N+1+vec_plot(2:end),k),-F(vec_plot(2:end),k),0.75,'Color','w','Linewidth',1)
                    
                    xlim([-5e-3 5e-3])
                    xlabel('x (m)')
                    ylim([-1e-3 3.5e-3])
                    ylabel('y (m)')
                    caxis([-1e-3 3e-3])
                    
                    subplot 222
                    hold on
                    eps_m = interp1(X,eps(find(rho>2e-3,1,'first'),:,k),x_m); %%PC densitiy=0.81/cm2
                    plot(X.*1e3,eps(find(rho>2e-3,1,'first'),:,k))
                    scatter(x_m.*1e3,eps_m,20,'k','filled')
                    ylim([-1 1])
                    xlabel('position (mm)')
                    ylabel('tangential strain')
                    
                    subplot 223
                    axis off
                    hold on
                    area(vecSurf,surfDef,-2e-3,'Facecolor','k')
                    z(:,k) = interp1(U(N+3:end,k)+x0(N+3:end),U(2:N+1,k)+x0(2:N+1),X);
                    pcolor(repmat(X,length(rho),1),flipud((z(:,k)+rho)'),eps(1+length(rho):end,:,k).*1e-3)
                    shading flat
                    axis equal
                    axis off
                    z_m = interp1(U(N+3:end,k)+x0(N+3:end),U(2:N+1,k)+x0(2:N+1)+2e-3,x_m); %%PC densitiy=0.81/cm2
                    scatter(x_m,z_m,20,'k','filled')
                    
                    quiver(U(N+1+vec_plot(2:end),k)+x0(N+1+vec_plot(2:end)),U(vec_plot(2:end),k)+x0(vec_plot(2:end)),...
                        -F(N+1+vec_plot(2:end),k),-F(vec_plot(2:end),k),0.75,'Color','w','Linewidth',1)
                    
                    xlim([-5e-3 5e-3])
                    xlabel('x (m)')
                    ylim([-1e-3 3.5e-3])
                    ylabel('y (m)')
%                     caxis([-1e-3 3e-3])
                    
                    subplot 224
                    hold on
                    eps_m = interp1(X,eps(length(rho)+find(rho>2e-3,1,'first'),:,k),x_m); %%PC densitiy=0.81/cm2
                    plot(X.*1e3,eps(length(rho)+find(rho>2e-3,1,'first'),:,k))
                    scatter(x_m.*1e3,eps_m,20,'k','filled')
                    ylim([-0.5 3])
                    xlabel('position (mm)')
                    ylabel('normal strain')
                    end
                    
                    drawnow
                    if(save==true)
                        % Capture the plot as an image
                        frame = getframe(h);
                        im = frame2im(frame);
                        [imind,cm] = rgb2ind(im,256);
                        if k==1
                            imwrite(imind,cm,filename,'gif','LoopCount',Inf,'DelayTime',1e-3);
                        else
                            imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',1e-3);
                        end
                    end
                    
                    pause(1e-6);
                end
            end
            
        end
        
        function ac = contactArea(Ftip,U)
            global x0
            surface = @(x) (sign(Ftip.Rcurv)*(sqrt(abs(Ftip.Rcurv)^2-x.^2)-abs(Ftip.Rcurv))).*(Ftip.Rcurv~=0) +...
                0.*(Ftip.Rcurv~=0);
            N = Ftip.Nl;
            for t=1:size(U,2)
                indC = find(U(1:N+1,t)+x0(1:N+1) - surface(U(N+2:2*(N+1),t)+x0(N+2:2*(N+1)))<=0);
                if(length(indC)>2)
                    ac(t) = U(max(indC)+N+1,t)+x0(max(indC)+N+1) - (U(min(indC)+N+1,t)+x0(min(indC)+N+1));
                end
            end
        end
        
        function delta = normIndent(Ftip,U)
            N = Ftip.Nl;
            delta = U(1,:)-U(floor(N/2)+2,:)+Ftip.r;
        end
        
        function div = divergence(Ftip,U)
            global x0
            surface = @(x) (sign(Ftip.Rcurv)*(sqrt(abs(Ftip.Rcurv)^2-x.^2)-abs(Ftip.Rcurv))).*(Ftip.Rcurv~=0) +...
                0.*(Ftip.Rcurv~=0);
            N = Ftip.Nl;
            indC = find(U(1:N+1,size(U,2))+x0(1:N+1) - surface(U(N+2:2*(N+1),size(U,2))+x0(N+2:2*(N+1)))<=0);
            for t=1:size(U,2)
                div2(:,t) = 2*gradient(U(N+3:2*(N+1),t),U(N+3:2*(N+1),t)+x0(N+3:2*(N+1)));
            end
            div = nanmedian(div2(:,:),1);
        end
        
        function [sedXY,sedXZ] = strainEnergy(Ftip,U)
            global x0
            surface = @(x) (sign(Ftip.Rcurv)*(sqrt(abs(Ftip.Rcurv)^2-x.^2)-abs(Ftip.Rcurv))).*(Ftip.Rcurv~=0) +...
                0.*(Ftip.Rcurv~=0);
            N = Ftip.Nl;
            E = Ftip.E_tis;
            indC = find(U(1:N+1,size(U,2))+x0(1:N+1) - surface(U(N+2:2*(N+1),size(U,2))+x0(N+2:2*(N+1)))<=0);
            for t=1:size(U,2)
                norm_strain(:,t) = diff(U(2:N+1,t));
                tan_strain(:,t) = diff(U(N+3:2*(N+1),t));
                udXY(:,t) = E*(1-Ftip.nu)/(2*(1+Ftip.nu)*(1-2*Ftip.nu))*(2*tan_strain(:,t).^2)+...
                    E*Ftip.nu/((1+Ftip.nu)*(1-2*Ftip.nu))*tan_strain(:,t).^2; %linear
                udXZ(:,t) = E*(1-Ftip.nu)/(2*(1+Ftip.nu)*(1-2*Ftip.nu))*(norm_strain(:,t).^2+2*tan_strain(:,t).^2)+...
                    E*Ftip.nu/((1+Ftip.nu)*(1-2*Ftip.nu))*tan_strain(:,t).^2.*norm_strain(:,t); %linear
                
                sedXY(:,t) = 0.002*nanmedian(udXY(:,t)).*1e3; %mJ/mm3
                sedXZ(:,t) = nanmedian(udXZ(:,t)).*1e6; %mJ/mm3
            end
        end
        
    end
end