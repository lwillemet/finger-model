# Finger model

This code provides a 2D parsimonious finite-difference model of human skin capturing the deformation of the skin,
viscoelastic effects through the damper, and local friction behavior at the interface.
The model is constructed with a bottom-up approach, using as few parameters to explain a wide array of observed phenomena.
It is composed of a chain of massless elements maintained together by elastic springs.
This chain can be assimilated to the external layer of the skin (the epidermis).
Its shape is maintained using other elastic springs that connect the massless elements to a virtual bone,
analogous to the mechanical behavior of the subcutaneous tissues.
The two elements on the outside of the membrane are also attached to the bone and model the effect of the rigid nail.
Overall, the model resemble the discrete version of a curved elastic membrane on a spring foundation.
Viscosity of the skin is modeled by dampers, connecting each particles to the mass of the system.

![model_connectivity](https://user-images.githubusercontent.com/58175648/115708505-61f45800-a370-11eb-9294-5a7a5baaf0d0.png)

External forces are applied on the bone element and the frictional forces are computed with the Dahl's friction model.
The displacements were solved using Runge-Kutta at order 4. For the equations, please refer to the submission.
